package cz.dubcat.animatedtitle;

import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class TitleMessageAPI {

    public enum Palette {
        ORANGE_PALETTE, RANDOM, LIGHT_PALLETE, LIGHT_RED, GREEN, LIGHT_GREEN, YELLOW, BLUE, GRAY, GOLD, AQUA;
    }

    Plugin plugin;

    public TitleMessageAPI(Plugin plugin) {
        this.plugin = plugin;
    }

    public void animatedTitleMessage(String text, Player p, Palette palette) {
        char[] wholetext = text.toCharArray();
        int delay = 0;

        for (int i = 0; i < wholetext.length; i++) {
            StringBuilder sb = new StringBuilder(selectPalette(palette) + "<");

            for (int y = 0; y <= i; y++) {
                sb.append(selectPalette(palette) + "" + wholetext[y]);
            }

            sb.append(selectPalette(palette) + ">");

            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                InternalAPI.sendTitle(p, sb.toString(), 0, 10, 0);
            }, delay);

            // increase delay
            delay += 2;
        }

    }

    public void animatedTitleMessage(List<String> textlist, Player p, Palette palette, int time_delay) {
        int global_delay = 0;

        for (String text : textlist) {
            char[] wholetext = text.toCharArray();
            int delay = 0;

            for (int i = 0; i < wholetext.length; i++) {
                StringBuilder sb = new StringBuilder(selectPalette(palette) + "<");

                for (int y = 0; y <= i; y++) {
                    sb.append(selectPalette(palette) + "" + wholetext[y]);
                }
                
                sb.append(selectPalette(palette) + ">");

                Bukkit.getScheduler().runTaskLater(plugin, () -> {
                    InternalAPI.sendTitle(p, sb.toString(), 0, 10, 0);
                }, (global_delay + delay));

                // increase delay
                delay += 2;

                if (i == wholetext.length - 1)
                    global_delay += delay;
            }

            global_delay += time_delay;
        }
    }

    public void animatedTitleMessage(String title, String subtitle, Player p, Palette palette_title, Palette sub_title,
            int time_delay) {

        int global_delay = 0;
        char[] wholetext = title.toCharArray();
        int delay = 0;

        for (int i = 0; i < wholetext.length; i++) {
            StringBuilder sb = new StringBuilder(selectPalette(palette_title) + "<");

            for (int y = 0; y <= i; y++) {
                sb.append(selectPalette(palette_title) + "" + wholetext[y]);
            }
            sb.append(selectPalette(palette_title) + ">");

            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                InternalAPI.sendTitle(p, sb.toString(), 0, 30, 0);
            }, (global_delay + delay));

            // increase delay
            delay += 2;

            if (i == wholetext.length - 1)
                global_delay += delay;
        }

        // SUBTITLE
        wholetext = subtitle.toCharArray();
        delay = 0;

        for (int i = 0; i < wholetext.length; i++) {
            StringBuilder sb = new StringBuilder(selectPalette(sub_title) + "<");

            for (int y = 0; y <= i; y++) {
                sb.append(selectPalette(sub_title) + "" + wholetext[y]);
            }

            sb.append(selectPalette(sub_title) + ">");

            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                InternalAPI.sendSubtitle(p, sb.toString(), 0, 10, 0);
            }, (global_delay + delay));

            // increase delay
            delay += 2;
        }

        global_delay += time_delay;

    }

    public int animatedTitleMessageWithSound(String title, String subtitle, Player p, Palette palette_title,
            Palette sub_title, int time_delay) {

        int global_delay = time_delay;
        char[] wholetext = title.toCharArray();
        int delay = 0;

        for (int i = 0; i < wholetext.length; i++) {
            StringBuilder sb = new StringBuilder(selectPalette(palette_title) + "<");

            for (int y = 0; y <= i; y++) {
                sb.append(selectPalette(palette_title) + "" + wholetext[y]);
            }

            sb.append(selectPalette(palette_title) + ">");

            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                InternalAPI.sendTitle(p, sb.toString(), 0, 150, 0);
                p.getLocation().getWorld().playSound(p.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 4);
            }, (global_delay + delay));

            // increase delay
            delay += 4;

            if (i == wholetext.length - 1)
                global_delay += delay;
        }

        // SUBTITLE
        wholetext = subtitle.toCharArray();
        delay = 0;

        for (int i = 0; i < wholetext.length; i++) {
            StringBuilder sb = new StringBuilder(selectPalette(sub_title) + "<");

            for (int y = 0; y <= i; y++) {
                sb.append(selectPalette(sub_title) + "" + wholetext[y]);
            }
            
            sb.append(selectPalette(sub_title) + "" + ">");
            
            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                InternalAPI.sendSubtitle(p, sb.toString(), 0, 100, 0);
                p.getLocation().getWorld().playSound(p.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 4);
            }, (global_delay + delay));

            // increase delay
            delay += 4;
        }

        global_delay += time_delay;

        return global_delay;

    }

    public void movingTitle(String title, Player p, Palette palette, double speed) {
        long global_delay = 0;
        char[] wholetext = title.toCharArray();
        int spaces = wholetext.length;

        for (int i = 0; i < wholetext.length; i++) {
            StringBuilder sb = new StringBuilder();

            for (int y = 0; y <= (spaces - i); y++) {
                sb.append(" ");
            }

            for (int y = 0; y <= i; y++) {
                sb.append(selectPalette(palette) + "" + wholetext[y]);
            }

            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                InternalAPI.sendTitle(p, sb.toString(), 0, 10, 0);
            }, (global_delay));

            global_delay += speed;

            if (i == (wholetext.length - 1)) {
                for (int y = 1; y <= wholetext.length; y++) {
                    StringBuilder sb2 = new StringBuilder();

                    for (int k = y; k < wholetext.length; k++) {
                        sb2.append(selectPalette(palette) + "" + wholetext[k]);
                    }

                    for (int k = 0; k <= y; k++) {
                        sb2.append(" ");
                    }
                    Bukkit.getScheduler().runTaskLater(plugin, () -> {
                        InternalAPI.sendTitle(p, sb2.toString(), 0, 10, 0);
                    }, (global_delay));

                    global_delay += speed;
                }
            }
        }
    }

    public void movingTitle(List<String> titlelist, Player p, Palette palette, double speed, Integer delay) {
        long global_delay = 0;

        for (String title : titlelist) {
            char[] wholetext = title.toCharArray();
            int spaces = wholetext.length;

            for (int i = 0; i < wholetext.length; i++) {
                StringBuilder sb = new StringBuilder();

                for (int y = 0; y <= (spaces - i); y++) {
                    sb.append(" ");
                }

                for (int y = 0; y <= i; y++) {
                    sb.append(selectPalette(palette) + "" + wholetext[y]);
                }

                Bukkit.getScheduler().runTaskLater(plugin, () -> {
                    InternalAPI.sendTitle(p, sb.toString(), 0, 10, 0);
                }, (global_delay));

                global_delay += speed;

                if (i == (wholetext.length - 1)) {
                    for (int y = 1; y <= wholetext.length; y++) {
                        StringBuilder sb2 = new StringBuilder();

                        for (int k = y; k < wholetext.length; k++) {
                            sb2.append(selectPalette(palette) + "" + wholetext[k]);
                        }

                        for (int k = 0; k <= y; k++) {
                            sb2.append(" ");
                        }
                        
                        Bukkit.getScheduler().runTaskLater(plugin, () -> {
                            InternalAPI.sendTitle(p, sb2.toString(), 0, 10, 0);
                        }, (global_delay));

                        global_delay += speed;
                    }
                }
            }

            global_delay += delay;
        }
    }

    public void lightenTitle(Player p, String title, ChatColor text_color, ChatColor light, int fadein, int stay,
            int fadeout) {
        long global_delay = 0;
        char[] wholetext = title.toCharArray();

        for (int i = 0; i < wholetext.length; i++) {
            StringBuilder sb = new StringBuilder();

            for (int y = 0; y <= wholetext.length; y++) {
                if (y == i)
                    sb.append(light + "" + wholetext[y]);
                else
                    sb.append(text_color + "" + wholetext[y]);
            }

            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                InternalAPI.sendTitle(p, sb.toString(), fadein, stay, fadeout);
            }, (global_delay));

            global_delay += 2;
        }
    }

    public void sentNormalTitle(Player p, String title, int fadein, int stay, int fadeout) {
        InternalAPI.sendTitle(p, title, fadein, stay, fadeout);
    }

    public void sentNormalSubtitle(Player p, String sub_title, int fadein, int stay, int fadeout) {
        InternalAPI.sendSubtitle(p, sub_title, fadein, stay, fadeout);
    }

    private ChatColor selectPalette(Palette p) {
        switch (p) {
        case ORANGE_PALETTE:
            return orangePallete();
        case RANDOM:
            return randColor();
        case LIGHT_RED:
            return ChatColor.RED;
        case LIGHT_PALLETE:
            return lightPallete();
        case GREEN:
            return ChatColor.DARK_GREEN;
        case LIGHT_GREEN:
            return ChatColor.GREEN;
        case YELLOW:
            return ChatColor.YELLOW;
        case BLUE:
            return ChatColor.BLUE;
        case GRAY:
            return ChatColor.GRAY;
        case GOLD:
            return ChatColor.GOLD;
        case AQUA:
            return ChatColor.AQUA;
        }

        return ChatColor.WHITE;
    }

    private ChatColor randColor() {
        int colorCode = new Random().nextInt(ChatColor.values().length);

        while (colorCode > 15) {
            colorCode = new Random().nextInt(ChatColor.values().length);
        }

        return ChatColor.values()[colorCode];

    }

    private ChatColor lightPallete() {
        int rand = 1 + (int) (Math.random() * 3);

        switch (rand) {
        case 1:
            return ChatColor.GREEN;
        case 2:
            return ChatColor.AQUA;
        case 3:
            return ChatColor.WHITE;
        }

        return ChatColor.WHITE;
    }

    private ChatColor orangePallete() {
        int rand = 1 + (int) (Math.random() * 3);

        switch (rand) {
        case 1:
            return ChatColor.YELLOW;
        case 2:
            return ChatColor.WHITE;
        case 3:
            return ChatColor.GOLD;
        }

        return ChatColor.WHITE;
    }

}
