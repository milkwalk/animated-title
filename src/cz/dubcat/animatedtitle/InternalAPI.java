package cz.dubcat.animatedtitle;

import java.lang.reflect.Constructor;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class InternalAPI {
    private static Class<?> PacketPlayOutChatClass;
    private static Class<?> IChatBaseComponentClass;
    private static Class<?> PacketPlayOutTitleClass;
    private static Class<?> PacketClass;
    
    public static void initialize() {
        PacketPlayOutChatClass = getNMSClass("PacketPlayOutChat");
        IChatBaseComponentClass = getNMSClass("IChatBaseComponent");
        PacketPlayOutTitleClass = getNMSClass("PacketPlayOutTitle");
        PacketClass = getNMSClass("Packet");
    }
    
    public static void sendPacket(Player player, Object packet) {
        try {
            Object handle = player.getClass().getMethod("getHandle").invoke(player);
            Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
            playerConnection.getClass().getMethod("sendPacket", PacketClass).invoke(playerConnection, packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Class<?> getNMSClass(String name) {
        String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        try {
            return Class.forName("net.minecraft.server." + version + "." + name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void sendActionBar(Player p, String message) {
        try {
            Object e;
            Object chatTitle;
            @SuppressWarnings("rawtypes")
            Constructor subtitleConstructor;
            Object titlePacket;

            e = PacketPlayOutChatClass.getDeclaredClasses()[0].getField("TITLE").get((Object) null);
            chatTitle = IChatBaseComponentClass.getDeclaredClasses()[0]
                    .getMethod("a", new Class[] { String.class })
                    .invoke((Object) null, new Object[] { "{\"text\":\"" + message + "\"}" });
            subtitleConstructor = PacketPlayOutChatClass.getConstructor(new Class[] {
                    PacketPlayOutChatClass.getDeclaredClasses()[0], IChatBaseComponentClass });
            titlePacket = subtitleConstructor.newInstance(new Object[] { e, chatTitle });
            sendPacket(p, titlePacket);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void sendTitle(Player p, String title, int fadein, int stay, int fadeout) {
        try {
            Object e;
            Object chatTitle;
            @SuppressWarnings("rawtypes")
            Constructor subtitleConstructor;
            Object titlePacket;

            if (title != null) {
                title = ChatColor.translateAlternateColorCodes('&', title);
                title = title.replaceAll("%player%", p.getDisplayName());
                // Times packets
                e = PacketPlayOutTitleClass.getDeclaredClasses()[0].getField("TIMES").get((Object) null);
                chatTitle = IChatBaseComponentClass.getDeclaredClasses()[0]
                        .getMethod("a", new Class[] { String.class })
                        .invoke((Object) null, new Object[] { "{\"text\":\"" + title + "\"}" });
                subtitleConstructor = PacketPlayOutTitleClass.getConstructor(new Class[] { 
                        PacketPlayOutTitleClass.getDeclaredClasses()[0],
                        IChatBaseComponentClass, Integer.TYPE, Integer.TYPE, Integer.TYPE });
                titlePacket = subtitleConstructor.newInstance(new Object[] { e, chatTitle, fadein, stay, fadeout });
                sendPacket(p, titlePacket);

                e = PacketPlayOutTitleClass.getDeclaredClasses()[0].getField("TITLE").get((Object) null);
                chatTitle = IChatBaseComponentClass.getDeclaredClasses()[0]
                        .getMethod("a", new Class[] { String.class })
                        .invoke((Object) null, new Object[] { "{\"text\":\"" + title + "\"}" });
                subtitleConstructor = PacketPlayOutTitleClass.getConstructor(new Class[] {
                        PacketPlayOutTitleClass.getDeclaredClasses()[0], IChatBaseComponentClass });
                titlePacket = subtitleConstructor.newInstance(new Object[] { e, chatTitle });
                sendPacket(p, titlePacket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendSubtitle(Player p, String subtitle, int fadein, int stay, int fadeout) {
        try {
            Object e;
            Object chatSubtitle;
            @SuppressWarnings("rawtypes")
            Constructor subtitleConstructor;
            Object subtitlePacket;

            if (subtitle != null) {
                subtitle = ChatColor.translateAlternateColorCodes('&', subtitle);
                subtitle = subtitle.replaceAll("%player%", p.getDisplayName());
                // Times packets
                e = PacketPlayOutTitleClass.getDeclaredClasses()[0].getField("TIMES").get((Object) null);
                chatSubtitle = IChatBaseComponentClass.getDeclaredClasses()[0]
                        .getMethod("a", new Class[] { String.class })
                        .invoke((Object) null, new Object[] { "{\"text\":\"" + subtitle + "\"}" });
                subtitleConstructor = PacketPlayOutTitleClass
                        .getConstructor(new Class[] { PacketPlayOutTitleClass.getDeclaredClasses()[0],
                                IChatBaseComponentClass, Integer.TYPE, Integer.TYPE, Integer.TYPE });
                subtitlePacket = subtitleConstructor
                        .newInstance(new Object[] { e, chatSubtitle, fadein, stay, fadeout });
                sendPacket(p, subtitlePacket);

                e = PacketPlayOutTitleClass.getDeclaredClasses()[0].getField("SUBTITLE").get((Object) null);
                chatSubtitle = IChatBaseComponentClass.getDeclaredClasses()[0]
                        .getMethod("a", new Class[] { String.class })
                        .invoke((Object) null, new Object[] { "{\"text\":\"" + subtitle + "\"}" });
                subtitleConstructor = PacketPlayOutTitleClass
                        .getConstructor(new Class[] { PacketPlayOutTitleClass.getDeclaredClasses()[0],
                                IChatBaseComponentClass, Integer.TYPE, Integer.TYPE, Integer.TYPE });
                subtitlePacket = subtitleConstructor
                        .newInstance(new Object[] { e, chatSubtitle, fadein, stay, fadeout });
                sendPacket(p, subtitlePacket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void clearTitle(Player player) {
        sendTitle(player, "", 0, 0, 0);
    }

}
