package cz.dubcat.animatedtitle;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    @Override
    public void onEnable() {
        getConfig().options().copyDefaults(true);
        saveConfig();
        InternalAPI.initialize();
    }
}
