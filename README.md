# Animated Title API

Titlebar api for bukkit/spigot


## Maven

### Repository

```xml
<repositories>
	<repository>
	    <id>jitpack.io</id>
	    <url>https://jitpack.io</url>
	</repository>
</repositories>
```

### Dependency

```xml
<dependency>
    <groupId>com.gitlab.milkwalk</groupId>
    <artifactId>animated-title</artifactId>
    <version>cc43d517c4</version>
</dependency>
```